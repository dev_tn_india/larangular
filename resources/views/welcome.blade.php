<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">    
        <!-- CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> 

        <!-- load bootstrap via cdn -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> 
        <!-- load fontawesome -->
        <!-- JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

        <!-- load angular -->
        
    <!-- ANGULAR -->
    <!-- all angular resources will be loaded from the /public folder -->
        <script src="js/controllers/mainCtrl.js"></script> <!-- load our controller -->
        <script src="js/services/commentService.js"></script> <!-- load our service -->
        <script src="js/app.js"></script> <!-- load our application -->
    


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-weight: 100;
                margin: 0;
                padding: 10px;
            }

            .m-b-md {
                margin-bottom: 30px;
                font-size: 24px;
                text-align: center;
                font-weight: bold;
            }
        </style>
    </head>
    <body ng-app="commentApp">


        <div ng-controller="commentCtrl">
            <div class="content">
                <div class="title m-b-md">
                    Laravel @ Angular
                </div>
             {{--    <p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p> --}}
                <div class="row">
                    <div class="col-md-8" >

                        <div class="comment" ng-hide="loading" ng-repeat="comment in comments">
                           <h3>Comment # @{{ comment.id }} <small>by @{{ comment.author }}</small> </h3>
                            <p> @{{ comment.content }}</p>
                            <p><a href="#" ng-click="deleteComment(comment.id)" class="text-muted">Delete</a></p>
                        </div>


                    </div>
                    <div class="col-md-4">
                        <form class="" ng-submit="submitComment()" style="position: fixed;">
                                                <!-- AUTHOR -->
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" name="author" ng-model="commentData.author" placeholder="Name">
                            </div>

                            <!-- COMMENT TEXT -->
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" name="comment" ng-model="commentData.content" placeholder="Say what you have to say">
                            </div>

                            <!-- SUBMIT BUTTON -->
                            <div class="form-group text-right">   
                                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                            </div>

                        </form>
                    </div>
                </div>
                
            </div>
        </div>
        <script>
angular.module('commentApp', ['comntController','commentService']);

</script>
    </body>
</html>
