<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
        public function index() {
        	return Comment::all();
        }
        public function store(Request $request) {
        	$comment = new Comment;
        	$comment->author = $request->author;
        	$comment->content = $request->content;
        	$comment->save();

        	return 'success';
        }
	    public function destroy($id) {
	        $b = Comment::find($id);
			$b->delete();

        	return 'success';
	    }
    	
}
